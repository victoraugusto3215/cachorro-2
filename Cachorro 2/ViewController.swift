//
//  ViewController.swift
//  Cachorro 2
//
//  Created by COTEMIG on 22/09/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Cachorro: Decodable {
    let message: String
    let status: String
}

class ViewController: UIViewController {

    @IBOutlet weak var img: UIImageView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getNovoCachoor()
}
    @IBAction func recarregarImagem(_sender: Any)
    {
        getNovoCachoor()
    }
    
    func getNovoCachoor()
    {
        AF.request("https://dog.ceo/api/breeds/image/random").responseDecodable(of: Cachorro.self)
            {
            response in
            if let cachorro = response.value
            {
                self.img.kf.setImage(with: URL(string: cachorro.message))
            }
        }
    }
}

